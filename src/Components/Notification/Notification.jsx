import React, { Component } from 'react'
import './style.css'


export class Notification extends Component {
  render() {
    return (
      <div className="notification">
        <div className="notification-date-badge">
          <div className="notification-date">10/20/2020</div>
        </div>
        <div className="notification-message">FAB LAB Meeting</div>
      </div>
    )
  }
}

export default Notification
