import React, { Component } from 'react'
import { Toast, ToastBody, Badge } from "reactstrap"

export class NotificationDisplay extends Component {
  render() {
    return (
      <div className="NotificationDisplay">
        <Toast>
          <Badge>10/11/2020</Badge>
          <ToastBody>FAB Lab Meeting</ToastBody>
        </Toast>
      </div>
    )
  }
}

export default NotificationDisplay
